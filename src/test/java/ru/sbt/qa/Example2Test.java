package ru.sbt.qa;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import ru.sbt.qa.impl.AnnotationMethod;
import ru.sbt.qa.runner.TestRunner;

/**
 * Created by smirnov on 09.05.17.
 */

@RunWith(TestRunner.class)
public class Example2Test {

    @Test
    @AnnotationMethod("4")
    public void testMethod4() throws Exception {
        Assert.assertTrue(true);
    }

    @Test
    @AnnotationMethod("5")
    public void testMethod5() throws Exception {
        Assert.assertTrue(false);
    }
}
