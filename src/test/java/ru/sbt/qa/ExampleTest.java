package ru.sbt.qa;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runner.RunWith;
import org.junit.runners.model.Statement;
import ru.sbt.qa.impl.AnnotationMethod;
import ru.sbt.qa.runner.TestRunner;

/**
 * Created by smirnov on 09.05.17.
 */

@RunWith(TestRunner.class)
public class ExampleTest {

    @Rule
    public TestWatcher watcher = new TestWatcher() {
        @Override
        public Statement apply(Statement base, Description description) {
            System.out.println("TestWatcher");
            return super.apply(base, description);
        }
    };

    @Before
    public void before(){
        System.out.println("before()");
    }

    @Test
    @AnnotationMethod("1")
    public void testMethod1() throws Exception {
        Assert.assertTrue(true);
    }

    @Test
    @AnnotationMethod("2")
    public void testMethod2() throws Exception {
        Assert.assertTrue(false);
    }

    @Before
    public void after(){
        System.out.println("after()");
    }

    @Test
    @AnnotationMethod("3")
    public void testMethod3() throws Exception {
        Assert.assertTrue(true);
    }
}
