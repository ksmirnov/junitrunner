package ru.sbt.qa.runner;

import org.junit.runner.Description;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import ru.sbt.qa.impl.AnnotationMethod;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import static java.lang.System.getProperty;

/**
 * Created by smirnov on 09.05.17.
 */
public class TestRunner extends BlockJUnit4ClassRunner {

    private Class clazz;
    private static List<String> tags = new ArrayList<String>();

    static {
        String tags = getProperty("tags");
        for (String tag : tags.split(",")) {
            TestRunner.tags.add(tag.trim());
        }
    }

    public TestRunner(Class clazz) throws InitializationError {
        super(clazz);
        this.clazz = clazz;
    }

    public Description getDescription() {
        return Description.createSuiteDescription(this.clazz.getName(),
                this.clazz.getAnnotations());
    }

    public void run(RunNotifier description) {
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            AnnotationMethod annotation = method.getAnnotation(AnnotationMethod.class);
            if (annotation != null && tags.contains(annotation.value())) {
                Description desc = Description.createTestDescription(clazz, method.getName());
                runLeaf(methodBlock(new FrameworkMethod(method)), desc, description);
            }
        }
    }
}
