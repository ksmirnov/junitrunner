package ru.sbt.qa.impl;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by smirnov on 09.05.17.
 */

@Retention(RetentionPolicy.RUNTIME)
public @interface AnnotationMethod {
    String value();
}
